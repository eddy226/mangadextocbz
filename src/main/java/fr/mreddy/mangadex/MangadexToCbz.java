package fr.mreddy.mangadex;

import java.io.File;
import java.util.List;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.mreddy.mangadex.client.contract.ClientWS;
import fr.mreddy.mangadex.client.impl.ClientWSImpl1;
import fr.mreddy.mangadex.model.Chapter;
import fr.mreddy.mangadex.model.Manga;
import fr.mreddy.mangadex.model.Result;

/**Console interface to search and download manga from the website MangaDex through the official API*/
public class MangadexToCbz 
{
	private static final String DOWNLOAD_FOLDER = "C:\\Temp\\Divers";
	//private static final String DOWNLOAD_FOLDER = "/home/mreddy/Téléchargements/comics";


	/**Research manga by title, author...*/
	private static void searchManga(Scanner sc, ClientWS clientWS) throws Exception
	{
		System.out.print("Search: ");
		String search = sc.nextLine();
		
		int fromResult;
		Result<Manga> result = null;
		do
		{
			if ( result == null )
				fromResult = 0;
			else
				fromResult = result.getNextPage();
			
			result = clientWS.search(search, fromResult);
			if ( result == null || result.getNbItemRequest() == 0 )
				System.out.println("No result for " + search);
			else
			{
				if ( fromResult == 1 )
					System.out.println( " - Total result " + result.getTotalSearch() );
				result.getItems().forEach( m -> System.out.println(" - " + m.getUid() + " : " + m.getTitle()) );
			}
		}
		while ( result != null && result.isNextPage() );
	}

	/**Display information about a manga from his uid*/
	private static void infoManga(Scanner sc, ClientWS clientWS) throws Exception
	{
		System.out.print("uid Manga: ");
		String search = sc.nextLine();
		
		Manga manga = clientWS.info(search);
		if ( manga == null )
			System.out.println("No result for uid " + search);
		else
		{
			System.out.println( "Title: " + manga.getTitle() );
			System.out.println( "Description: " + manga.getDescription() );
			System.out.println( "Year: " + manga.getYear() );
			System.out.println( "LastChapter: " + manga.getLastChapter() );
			System.out.println( "LastVolume: " + manga.getLastVolume() );
			System.out.println( "Status: " + manga.getStatus() );

			String userMenu;
			do
			{
				printMenuManga(manga, null);
				userMenu = sc.nextLine();
				if ( ! userMenu.isBlank() && ! userMenu.isEmpty() )
				{
					switch (userMenu)
					{
						case "c":
						case "C":
							listChapter(sc, clientWS, manga);
							break;
						case "dc":
						case "DC":
							downloadChapter(sc, clientWS, manga);
							break;
						case "dv":
						case "DV":
							downloadVolume(sc, clientWS, manga);
							break;
						case "q":
						case "Q":
							System.out.println("Return to main menu");
							break;
						default:
							System.out.println("Unknown command " + userMenu);
					}
				}
			}
			while ( ! "q".equals(userMenu) && ! "Q".equals(userMenu) );
		}
	}
	
	/**List all chapters of a manga*/
	private static void listChapter(Scanner sc, ClientWS clientWS, Manga manga) throws Exception
	{
		int fromResult = 0;
		String userMenu = null;
		
		do
		{
			Result<Chapter> result = clientWS.listChapter(manga, fromResult);
			if ( result == null || result.getNbItemRequest() == 0 )
			{
				System.out.println("No chapter available for " + manga.getTitle());
				userMenu = "Q";
			}
			else
			{
				System.out.println( " - total " + result.getTotalSearch() );
				if ( result.getTotalSearch() > 0 )
				{
					result.getItems().forEach( c -> System.out.println( " - Volume " + c.getVolume() + ", chapter " + c.getChapter() + " : " + c.getTitle() ) );
					
					printMenuManga(manga, result);
					userMenu = sc.nextLine();
					if ( ! userMenu.isBlank() && ! userMenu.isEmpty() )
					{
						switch (userMenu)
						{
							case "c":
							case "C":
								fromResult += result.getNbItemRequest();
								break;
							case "dc":
							case "DC":
								downloadChapter(sc, clientWS, manga);
								break;
							case "dv":
							case "DV":
								downloadVolume(sc, clientWS, manga);
								break;
							case "q":
							case "Q":
								break;
							default:
								System.out.println("Unknown command " + userMenu);
						}
					}
				}
			}
		}
		while ( ! "q".equals(userMenu) && ! "Q".equals(userMenu) );
	}
	
	private static void downloadChapter(Scanner sc, ClientWS clientWS, Manga manga) throws Exception
	{
		System.out.print("Chapter number to donwload: ");
		String userSelection = sc.nextLine();

		int numChapter = Integer.parseInt(userSelection);
		Result<Chapter> result = clientWS.chapter(manga, numChapter);
		if ( result == null || result.getNbItemRequest() == 0 )
			System.out.println("Aucun chapitre ne correspond");
		else
			generateFile(clientWS, manga, result.getItems());	
	}

	private static void downloadVolume(Scanner sc, ClientWS clientWS, Manga manga) throws Exception
	{
		System.out.print("Volume number to donwload: ");
		String userSelection = sc.nextLine();

		int numVolume = Integer.parseInt(userSelection);
		
		int fromResult;
		Result<Chapter> result = null;
		do
		{
			if ( result == null )
				fromResult = 0;
			else
				fromResult = result.getNextPage();
			result = clientWS.volume(manga, numVolume, fromResult);
		
			if ( result == null || result.getNbItemRequest() == 0 )
				System.out.println("No Chapter / Volume available");
			else
				generateFile(clientWS, manga, result.getItems());
		}
		while ( result != null && result.isNextPage() );
	}
	
	private static void generateFile(ClientWS clientWS, Manga manga, List<Chapter> lstChapter) throws Exception
	{
		for ( Chapter chapter : lstChapter )
		{
			System.out.println( " - Generation of volume " + chapter.getVolume() + " chapter " + chapter.getChapter() + " : '" + chapter.getTitle() + "' with " + chapter.getPages().size() + " pages");
			
			File cbzFile = clientWS.generateFile(DOWNLOAD_FOLDER, manga, chapter);
			System.out.println("Recorded on " + cbzFile.getAbsolutePath());
		}
	}

	private static void printMainMenu()
	{
		System.out.println(" --- Main Menu --- ");
		System.out.println("S : Search for your manga");
		System.out.println("I : Manga Information");
		System.out.println("Q : Quit application");
	}

	private static void printMenuManga(Manga manga, Result<Chapter> result)
	{
		System.out.println(" --- Manga Menu " + manga.getTitle() + " --- ");
		if ( result == null )
			System.out.println("C : List all chapters");
		else if ( result.isNextPage() )
			System.out.println("C : Next chapters");
		System.out.println("DC : Download a chapter");
		System.out.println("DV : Download a volume");
		System.out.println("Q : Quit to main menu");
	}

	public static void main(String[] args) throws Exception 
	{
		// Init logging
		Logger logger = Logger.getGlobal();
		logger.setLevel(Level.ALL);
		
		Handler handler = new ConsoleHandler();
		handler.setLevel(Level.ALL);
		logger.addHandler(handler);
		logger.setUseParentHandlers(false);
		
		logger.log(Level.FINEST, "finest");
		logger.log(Level.INFO, "info");
		logger.log(Level.SEVERE, "severe");
		
		// Webservice Client
		ClientWS clientWS = new ClientWSImpl1(logger);
		
		// Menu
		String userMenu;
		Scanner sc = new Scanner(System.in);
		do
		{
			printMainMenu();
			userMenu = sc.nextLine();
			if ( ! userMenu.isBlank() && ! userMenu.isEmpty() )
			{
				switch (userMenu)
				{
					case "s":
					case "S":
						searchManga(sc, clientWS);
						break;
					case "i":
					case "I":
						infoManga(sc, clientWS);
						break;
					case "q":
					case "Q":
						System.out.print("Bye bye");
						break;
					default:
						System.out.println("Unknown command " + userMenu);
				}
			}
		}
		while ( ! "q".equals(userMenu) && ! "Q".equals(userMenu) );
		
		sc.close();
	}
}

