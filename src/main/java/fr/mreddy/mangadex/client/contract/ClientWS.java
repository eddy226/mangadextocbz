package fr.mreddy.mangadex.client.contract;

import java.io.File;

import fr.mreddy.mangadex.model.Chapter;
import fr.mreddy.mangadex.model.Manga;
import fr.mreddy.mangadex.model.Result;

public interface ClientWS
{
	/**Search all manga corresponding to the keyword in the API with pagination*/
	Result<Manga> search(String keyword, int fromResult) throws Exception;
	/**Get information about one manga from his uid*/
	Manga info(String uidManga) throws Exception;
	/**Get the list of chapters of a manga*/
	Result<Chapter> listChapter(Manga manga, int numPage) throws Exception;
	/**Get the list of chapters corresponding to the chapter number and the manga * 
	 * ( Can return more than one items )*/
	Result<Chapter> chapter(Manga manga, int numChapter) throws Exception;
	/**Get the list of chapters corresponding to the volume number and the manga*/
	Result<Chapter> volume(Manga manga, int numVolume, int fromResult) throws Exception;
	/**Download and generate a Cbz file containing the chapter*/
	File generateFile(String folder, Manga manga, Chapter chapter) throws Exception;
}
