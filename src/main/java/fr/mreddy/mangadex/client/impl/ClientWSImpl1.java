package fr.mreddy.mangadex.client.impl;

import static java.net.HttpURLConnection.HTTP_OK;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.mreddy.mangadex.client.contract.ClientWS;
import fr.mreddy.mangadex.model.Chapter;
import fr.mreddy.mangadex.model.Manga;
import fr.mreddy.mangadex.model.Result;

public class ClientWSImpl1 implements ClientWS 
{
	private static final String BASE_URL_WS = "https://api.mangadex.org/";
	private static final String BASE_URL_SCAN = "https://uploads.mangadex.org/data/";
	
	private NumberFormat nf;
	/**Object Mapper*/
	private ObjectMapper mapper;
	
	private Logger logger;
	
	public ClientWSImpl1(Logger logger)
	{
		this.logger = logger;

		this.nf = NumberFormat.getIntegerInstance();
		this.nf.setMinimumIntegerDigits(3);
		
		this.mapper = new ObjectMapper();
	}
	
	/**Get api url dependings on the para
	 * meters*/
	private HttpURLConnection getConnection(String urlParam) throws Exception
	{
		URL urlWS = new URL(BASE_URL_WS + urlParam);
		
		logger.fine("Call url: " + urlWS);

		// Création de l'objet connexion
		HttpURLConnection connection = (HttpURLConnection)urlWS.openConnection();
	    
		// Paramétrage de la connexion
	    connection.setRequestProperty("Content-Type", "application/json; charset=" + Constantes.ENCODAGE);
	    connection.setRequestProperty("Accept", "application/json");

	    return connection;
	}
	
	@Override
	public Result<Manga> search(String keyword, int fromResult) throws Exception
	{
		JsonNode node = getJSonNode("manga?title=" + URLEncoder.encode(keyword.trim(), Constantes.ENCODAGE) + "&limit=100&offset=" + fromResult);	// "order[createdAt]=asc"
		if ( node == null )
			return null;
		else
		{
			int totalResult = node.get("total").asInt();
			List<Manga> lstManga;
			
			if ( totalResult == 0 )
				lstManga = null;
			else
			{
				lstManga = new LinkedList<>();
			
				Iterator<JsonNode> iteratorManga = node.get("results").iterator();
				while ( iteratorManga.hasNext() )
					lstManga.add( new Manga(iteratorManga.next().get("data")) );
			}
			
			return new Result<Manga>(totalResult, fromResult, lstManga);
		}
	}
	
	@Override
	public Manga info(String uidManga) throws Exception
	{
		JsonNode node = getJSonNode("manga/" + URLEncoder.encode(uidManga.trim(), Constantes.ENCODAGE));
		if ( node == null )
			return null;
		else
			return new Manga( node.get("data") );
	}
		
	@Override
	public Result<Chapter> listChapter(Manga manga, int fromResult) throws Exception
	{
		JsonNode node = getJSonNode("chapter?manga=" + URLEncoder.encode(manga.getUid(), Constantes.ENCODAGE) + "&translatedLanguage[]=en&order[chapter]=asc&limit=100&offset=" + fromResult);
		if ( node == null )
			return null;
		else
			return getResultChapter(node, fromResult, false);
	}

	@Override
	public Result<Chapter> chapter(Manga manga, int numChapter) throws Exception
	{
		JsonNode node = getJSonNode("chapter?manga=" + URLEncoder.encode(manga.getUid(), Constantes.ENCODAGE) + "&chapter=" + numChapter + "&translatedLanguage[]=en&order[volume]=asc");
		if ( node == null )
			return null;
		else
			return getResultChapter(node, 1, true);
	}
	
	@Override
	public Result<Chapter> volume(Manga manga, int numVolume, int fromResult) throws Exception
	{
		JsonNode node = getJSonNode("chapter?manga=" + URLEncoder.encode(manga.getUid(), Constantes.ENCODAGE) + "&volume=" + numVolume + "&translatedLanguage[]=en&offset=" + fromResult);
		if ( node == null )
			return null;
		else
			return getResultChapter(node, fromResult, true);
	}
	
	/**Extract datas from Json response to Result of Chapter*/
	private Result<Chapter> getResultChapter(JsonNode node, int fromResult, boolean loadPages)
	{
		int totalResult = node.get("total").asInt();
		List<Chapter> lstChapter;
	
		if ( totalResult == 0 )
			lstChapter = null;
		else
		{
			lstChapter = new LinkedList<>();
		
			Iterator<JsonNode> iteratorManga = node.get("results").iterator();
			while ( iteratorManga.hasNext() )
			{
				JsonNode nodeAttribute = iteratorManga.next().get("data").get("attributes");
				Chapter chapter = new Chapter(nodeAttribute);
				if ( loadPages )
					chapter.setPages( nodeAttribute.get("data") );
				lstChapter.add( chapter );
			}
		}
	
		return new Result<Chapter>(totalResult, fromResult, lstChapter);
	}

	@Override
	public File generateFile(String folder, Manga manga, Chapter chapter) throws Exception
	{
		String fileName = manga.getTitle() + " - v" + nf.format(chapter.getVolume()) + " - c" + nf.format(chapter.getChapter()) + " " + chapter.getTitle();  
		// remove unwanted character
		File cbzFileDestination = new File(folder + "/" + fileName.replaceAll("[:\\\\/*?|<>]", "_") + ".cbz");

		// Compress pages
		FileOutputStream fos = new FileOutputStream(cbzFileDestination);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		
		for ( String pageName : chapter.getPages() )
		{
			logger.info("Page Treatment " + pageName);
			
			ZipEntry zipEntry = new ZipEntry(pageName);
			zipOut.putNextEntry(zipEntry);
			
			URL urlDownload = new URL(BASE_URL_SCAN + pageName);
			URLConnection connectionDownload = urlDownload.openConnection();
			
			try ( InputStream in = connectionDownload.getInputStream() )
			{
			    in.transferTo(zipOut);
			}
			zipOut.closeEntry();
		}
		zipOut.close();
		fos.close();

	    return cbzFileDestination;
	}
	
	private JsonNode getJSonNode(String urlParam) throws Exception
	{
		HttpURLConnection urlConnection = getConnection(urlParam);
		
		JsonNode result;
	    if( urlConnection.getResponseCode() == HTTP_OK )
	    	//readAll(urlConnection, Level.INFO);
	    	result = mapper.readTree(urlConnection.getInputStream());
	    else
	    {
	    	logger.severe("HTTP error " + urlConnection.getResponseCode() + ": " + urlConnection.getResponseMessage());
	    	result = null;
	    }
		urlConnection.disconnect();
		
		return result;
	}
	
	/**DEBUG*/
	/*private void readAll(URLConnection connection, Level levelAffichage) throws IOException 
	{
		if ( logger.getLevel() == Level.ALL || logger.getLevel().intValue() >= levelAffichage.intValue() )
		{
			try ( InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
					BufferedReader reader = new BufferedReader(inputStreamReader) )
			{
				final int MAX_LOG = 20000;
				
				String line = null;
				while ( (line = reader.readLine()) != null )
				{
					if ( line.length() >= MAX_LOG)
						line = line.substring(0, MAX_LOG - 3) + "...";
					logger.log(levelAffichage, line);
				}

			}
		}
	}*/
}
