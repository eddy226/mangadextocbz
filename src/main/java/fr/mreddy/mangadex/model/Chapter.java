package fr.mreddy.mangadex.model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**Chapter of a manga*/
public class Chapter
{
	private int volume;
	private int chapter;
	private String title;
	private String hash;
	private List<String> lstPages;
	
	public Chapter(JsonNode node)
	{
		this.volume = node.get("volume").asInt();
		this.chapter = node.get("chapter").asInt();
		this.title = node.get("title").asText();
		this.hash = node.get("hash").asText();
	}

	public int getVolume()
	{
		return volume;
	}

	public int getChapter()
	{
		return chapter;
	}

	public String getTitle()
	{
		return title;
	}
	
	public void setPages(JsonNode nodeData)
	{
		lstPages = new LinkedList<>();

		Iterator<JsonNode> iteratorPage = nodeData.iterator();
		while ( iteratorPage.hasNext() )
			lstPages.add( hash + "/" + iteratorPage.next().asText() );
	}

	public List<String> getPages()
	{
		return lstPages;
	}
}
