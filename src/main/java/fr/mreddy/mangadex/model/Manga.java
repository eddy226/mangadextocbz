package fr.mreddy.mangadex.model;

import com.fasterxml.jackson.databind.JsonNode;

public class Manga 
{
	private String uid;
	private String title;
	private String description;
	private int year;
	private int lastChapter;
	private int lastVolume;
	private String status;
	
	
	public Manga(JsonNode node) 
	{
		this.uid = node.get("id").asText();
		
		JsonNode nodeAttribute = node.get("attributes");
		this.title = getLanguage( nodeAttribute.get("title") );
		this.description = getLanguage( nodeAttribute.get("description") );
		this.year = nodeAttribute.get("year").asInt();
		this.lastChapter = nodeAttribute.get("lastChapter").asInt();
		this.lastVolume = nodeAttribute.get("lastVolume").asInt();
		this.status = nodeAttribute.get("status").asText();
	}

	public String getUid() 
	{
		return uid;
	}

	public String getTitle() 
	{
		return title;
	}

	public String getDescription() 
	{
		return description;
	}
	
	public int getYear()
	{
		return year;
	}

	public int getLastChapter() 
	{
		return lastChapter;
	}
	
	public int getLastVolume() 
	{
		return lastVolume;
	}

	public String getStatus() 
	{
		return status;
	}

	private String getLanguage(JsonNode node) 
	{
		if (node.get("en") != null)
			return node.get("en").asText();
		else
			return "test other language " + node;
	}
}
