package fr.mreddy.mangadex.model;

import java.util.List;

public class Result<T>
{
	private int fromResult;
	private int totalResult;
	private List<T> lstItem;

	public Result(int totalResult, int fromResult, List<T> lstItem)
	{
		this.totalResult = totalResult;
		this.fromResult = fromResult;
		this.lstItem = lstItem;
	}

	public int getNbItemRequest()
	{
		if ( lstItem == null )
			return 0;
		else
			return lstItem.size();
	}

	public int getTotalSearch()
	{
		return totalResult;
	}
	
	public List<T> getItems()
	{
		return lstItem;	
	}

	public boolean isNextPage()
	{
		return totalResult > getNextPage();
	}

	public int getNextPage()
	{
		return fromResult + getNbItemRequest();
	}
}
